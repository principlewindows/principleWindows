// The service worker should include the following:
//  1. Version number (or other identifier)
//  2. Cache version name
//  3. List of resources to cache
//  4. Installing the cache when the app is installed
//  5. Updating itself and the other application file as needed
//  6. Removing cached files that are no longer used
//  7. The above tasks are archieved by reacting to three service worker events:
//      - fetch, install, and activate events

//  1. Version number (or other identifier)
const _JICHENG_VERSION_CONTROL_SW = 0;
//  2. Cache version name
const CACHE_NAME = `Jicheng-PRINCIPLE_WINDOWS-v${_JICHENG_VERSION_CONTROL_SW}`;
//const CACHE_NAME = `Jicheng-CNKID-v0`;

//  3. List of resources to cache
//  Use the install event to pre-cache all initial resources.
self.addEventListener('install', event => {
    event.waitUntil((async () => {
        const cache = await caches.open(CACHE_NAME);
        cache.addAll([
            '/',
            '/manifest.json',
            '/favicon.ico',
            '/stars.png',
        ]);
    })());
});

// This function returns a 'live' resource corresponding to the request_url
async function live_update_asset(request_url)
{
    // try to fetch it from the network with a version trick
    let request_ex = "";

    request_ex += request_url;
    request_ex += "?version=";
    request_ex += new Date().getTime();

    // try to directly fetch the 'live' resource from the network
    try {
        fetch(request_ex)
            .then((response) => {
                console.log(
                    "fetch(request_url):",
                    response.url,
                );

                return cacheRequestResponse(request_url, response);
            })
            .catch((error) => {
                console.error(
                    "\tfetch error:", error
                );
            });
    } catch (error) {
        console.error("There has been a problem with the fetch operation:", error);
    }

    // now that we failed to get resource from the network
    // we seek to cached resources to find one for return
    const cache = await caches.open(CACHE_NAME);
    const cachedResponse = await cache.match(request_url);
    if (cachedResponse) {
        // Cache hit - return response

        console.log(
            "\tSucceeded to fetch the resource from the cache:",
            cachedResponse.url,
        );

        return cachedResponse;
    } else {
        console.log(
            "\tfailed to fetch the resource from the cache:",
            request_url,
            "be4 that we failed to fetch from the network.",
        );
    }
}

// Here's the fetching listener
self.addEventListener('fetch', event => {
    let request_url = "";

    request_url += event.request.url;

    // when seeking an HTML page
    if (event.request.mode === "navigate") {
        console.log(
            "event.request.mode === \"navigate\"",
            "\r\n\t",
            request_url,
        );

        //event.respondWith(caches.match("/"));

        //const liveResponse = live_update_asset(request_url);
        //event.respondWith(liveResponse);
        event.respondWith(live_update_asset(request_url));

        return;
    }

    // For other 'live' resources, we request with time stamp
    if (request_url.includes("_live")) {
        console.log(
            "fetch live asset:\r\n\t",
            request_url,
        );

        event.respondWith(live_update_asset(request_url));

        return;
    }

    // for 'lazy' resource
    if (request_url.includes("_lazy")) {
        return;
    }

    // For every other request type
    event.respondWith((async () => {
        const cache = await caches.open(CACHE_NAME);

        // Get the resource from the cache.
        const cachedResponse = await cache.match(request_url);
        if (cachedResponse) {
            //event.respondWith(cachedResponse);
            return cachedResponse;
        } else {
            try {
                // If the resource was not in the cache, try the network.
                const fetchResponse = await fetch(event.request);

                // Save the resource in the cache and return it.
                cache.put(event.request, fetchResponse.clone());

                //event.respondWith(fetchResponse);
                return fetchResponse;
            } catch (error) {
                // The network failed.
                console.error("network fetch error after dismatch from the cache:\r\n\t", error);
            }
        }
    })());
});

//   When an existing service worker is being replaced by a new one,
// the existing service worker is used as the PWA's service worker
// until the new service worker is activated.
//   We use the activate event to delete old caches to avoid running
// out of space.
//   We listen for the current service worker's global scope activate event.
self.addEventListener("activate", (event) => {
    event.waitUntil(
        (async () => {
            const names = await caches.keys();
            await Promise.all(
                names.map((name) => {
                    if (name !== CACHE_NAME) {
                        return caches.delete(name);
                    }
                })
            );
            await clients.claim();
        })()
    );
});

//This function will return files from the local cache.
//If their name ends with ��.updated.js�� return a network response, 
//aka the browser will ask the actual server for the contents of the file.
function cacheRequestResponse(request, response) {
    // Check if we received a valid response
    if (!response || response.status !== 200 || response.type !== 'basic') {

        console.log(
            "fetch error!!!",
        );

        return response;
    }

    // IMPORTANT: Clone the response. A response is a stream
    // and because we want the browser to consume the response
    // as well as the cache consuming the response, we need
    // to clone it so we have two streams.
    let responseToCache = response.clone();

    console.log(
        "\t==> cache the fetch result:",
        response.url,
    );

    caches.open(CACHE_NAME)
        .then((cache) => {
            cache.put(request, responseToCache);
        });

    return responseToCache;
}
